package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public final class DBManager {
    public static final String SELECT_ALL_USERS = "SELECT * FROM users";
    public static final String SELECT_USER_BY_LOGIN = "SELECT  * FROM  users where login = (?)";
    public static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    public static final String DELETE_FROM_USERS = "DELETE FROM users where login in (?)";
    public static final String INSERT_INTO_TEAMS = "INSERT INTO teams (name) VALUES (?)";
    public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_TEAMS_BY_NAME = "SELECT * FROM teams WHERE name = (?)";
    public static final String DELETE_FROM_TEAMS_BY_NAME = "DELETE FROM teams WHERE name = (?)";

    private Connection getConnection() throws SQLException {
        return getConnection(true);
    }

    private Connection getConnection(boolean autocommit) throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db",
                "root", "0607");
        System.out.println("Connection OK"); // Проверка соединения
        con.setAutoCommit(autocommit);
        con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return con;
    }

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        try (Connection con = getConnection()) {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SELECT_ALL_USERS);
            List<User> users = new ArrayList<>();
            while (rs.next()) {
                users.add(mapUser(rs));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Can not get users", e);
        }
    }

    public boolean insertUser(User user) throws DBException {

        try (Connection con = getConnection()) {
            PreparedStatement st = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, user.getLogin());
            st.executeUpdate();
            ResultSet keys = st.getGeneratedKeys();
            if (keys.next()) {
                return 0 == keys.getInt(1);
            }
            return false;
//            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DB ERROR", e);
        }

    }

    public boolean deleteUsers(User... users) throws DBException {
        PreparedStatement preparedStatement;
        Connection con = null;
        try {
            con = getConnection();
            preparedStatement = con.prepareStatement(DELETE_FROM_USERS);
            for (User us : users) {
                preparedStatement.setString(1, us.getLogin());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DBException("bad", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {

        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_USER_BY_LOGIN)) {
            st.setString(1, login);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return mapUser(rs);
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Can not get users", e);
        }
//        return null;
    }

    private User mapUser(ResultSet rs) throws SQLException {
        User a = new User();
        a.setId(rs.getInt("id"));
        a.setLogin(rs.getString("login"));
//        System.out.println(a);
        return a;
    }

    public Team getTeam(String name) throws DBException {
        Team t = new Team();
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement(SELECT_TEAMS_BY_NAME)) {
            st.setString(1, name);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    t.setId(rs.getInt("id"));
                    t.setName(rs.getString("name"));
                    System.out.println(t);
                    System.out.println(t.getId());
                    return t;
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Can not get users", e);
        }
//        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection con = getConnection()) {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(SELECT_ALL_TEAMS);
            List<Team> teams = new ArrayList<>();
            while (rs.next()) {
                Team t = new Team();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                teams.add(t);
            }
            return teams;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Can not get teams", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = getConnection()) {
            PreparedStatement st = con.prepareStatement(INSERT_INTO_TEAMS, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, team.getName());
            st.executeUpdate();
            ResultSet keys = st.getGeneratedKeys();
            if (keys.next()) {
                return 0 == keys.getInt(1);
            }
            return false;
        } catch (SQLException e) {
            throw new DBException("DB ERROR", e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        User user1 = getUser(user.getLogin());
        try (Connection con = getConnection()) {
            for (Team ttt : teams) {
                ttt = getTeam(ttt.getName());
                PreparedStatement st = con.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES" + " (" +
                        user1.getId() + "," + ttt.getId() + ")");
                st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        User user1 = getUser(user.getLogin());
        List<Team> teamsID = new ArrayList<>();
        try (Connection con = getConnection()) {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT team_id FROM users_teams WHERE user_id =" + user1.getId());
            while (rs.next()) {
                Team t = new Team();
                t.setId(rs.getInt("team_id"));
                teamsID.add(t);
            }
            List<Team> teamsNames = new ArrayList<>();
            Statement st2 = con.createStatement();
            for (Team tttt : teamsID) {
                ResultSet rs2 = st2.executeQuery("SELECT name FROM teams where id =" + tttt.getId());
                while (rs2.next()) {
                    Team t2 = new Team();
                    t2.setName(rs2.getString("name"));
                    teamsNames.add(t2);
                }
            }
            teamsNames.forEach(System.out::println);
            return teamsNames;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Can not get teams", e);
        }

    }

    public boolean deleteTeam(Team team) throws DBException {
        PreparedStatement preparedStatement;
        Connection con = null;
        try {
            con = getConnection();
            preparedStatement = con.prepareStatement(DELETE_FROM_TEAMS_BY_NAME);
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("bad", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = getConnection();
             PreparedStatement st = con.prepareStatement("UPDATE teams SET name = (?) WHERE id =" + team.getId())) {
            st.setString(1, team.getName());
            st.executeUpdate();

            return true;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

    }
}
